<?php
session_start();
include __DIR__."/../vendor/autoload.php";

Tracy\Debugger::enable();

Flash::options(["element"=>"p","types" => ["success" => ["icon" => "fa fa-check"],"danger"=>["icon"=>"fa fa-times"]]]);

Flash::success("Success message");
Flash::danger("Danger message");
Flash::warning("Warning message");
Flash::info("Info message");

if($_GET["message"]){
	\Kubomikita\Flash::success($_GET["message"],"show.php");
}



?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


<div class="container">
	<?php echo Flash::display();?>

	<a href="?message=Správa :-)">Nastaviť správu a redirect!</a>
</div>