<?php
namespace Kubomikita;
/**
 * Obslužná trieda pre predavanie, spracovanie a vypisovanie chybových hláseni
 *
 * @author Jakub Mikita
 * @year 2018
 * @version 1.0
 *
 * @method static success(string $message, string $redirect = null)
 * @method static danger(string $message, string $redirect = null)
 * @method static info(string $message, string $redirect = null)
 * @method static warning(string $message, string $redirect = null)
 */
class Flash {

	const GENERATOR = "kubomikita/flash";
	
	const TYPE_SUCCESS = 'success';
	const TYPE_DANGER = 'danger';
	const TYPE_INFO = 'info';
	const TYPE_WARNING = 'warning';

	private static $options = [
		"element" => 'div',
		"elementClass" => "alert",
		"types" => [
			"success" => [
				"class" => "alert-success",
			],
			"danger" => [
				"class" => "alert-danger",
			],
			"info" => [
				"class" => "alert-info",
			],
			"warning" => [
				"class" => "alert-warning",
			]

		]
	];


	/**
	 * Pridá do session správu, ak treba redirectne
	 *
	 * @param string $name
	 * @param string $message
	 * @param string $redirect
	 *
	 * @throws \ErrorException
	 */
	public static function add($name, $message, $redirect = null) {
		if(strlen($message) === 0){
			throw new \ErrorException("Please enter a message.");
		}
		$_SESSION[self::GENERATOR]['flash'][] = array("type" => $name, "text" => $message);
		if ($redirect !== null) {
			header("location: " . $redirect);
			exit();
		}
	}

	/**
	 * Urobi to ze ak zavolas Flash::success($param) tak vlastne zavola Flash::add("success",$param)
	 *
	 * @param string $fn
	 * @param array $args
	 */
	public static function __callStatic($fn, $args) {
		call_user_func_array(array('Flash', 'add'), array($fn, $args[0], (isset($args[1])?$args[1]:null)));
	}

	/**
	 * Vymaze vsetky správy zo sessions
	 */
	public static function flushAll() {
		$_SESSION[self::GENERATOR]['flash'] = [];
	}

	public static function isset($type) {
		if(isset($_SESSION[self::GENERATOR]["flash"]) && !empty($_SESSION[self::GENERATOR]["flash"])) {
			foreach ( $_SESSION[ self::GENERATOR ]["flash"] as $flash ) {
				if ( $flash["type"] === $type ) {
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * Zobrazovacia metoda správ, podla toho co chceme zobrazit to zobrazi podla predanej konsanty
	 *
	 */
	public static function display() {
		if (session_status() == PHP_SESSION_NONE) {
			throw new \ErrorException("Session must by started!");
		}

		if(isset($_SESSION[self::GENERATOR]["flash"]) && !empty($_SESSION[self::GENERATOR]["flash"])){
			$o = self::$options;
			foreach ($_SESSION[self::GENERATOR]["flash"] as $key => $value) {
				$text = $value["text"];
				$valueClass = $icon = "";
				if(isset($o["types"][$value["type"]])){
					$type = $o["types"][$value["type"]];
					$valueClass = $type["class"];
					if(isset($type["icon"])){
						$icon = '<i class="'.$type["icon"].'"></i> ';
					}
					if(isset($type["image"])){
						//$icon = 'image '.$type["image"];
						$icon = '<img src="'.$type["image"].'" style="margin-bottom: -3px;"> ';
					}
				}
				echo '<'.$o["element"].' class="'.$o["elementClass"].' '.$valueClass.'" '.(isset($o["types"][$value["type"]]["style"])?'style="'.$o["types"][$value["type"]]["style"].'"':'').'>'.$icon.$text.'</'.$o["element"].'>';
				unset($_SESSION[self::GENERATOR]["flash"]);
			}
		}

	}

	public static function options($options){
		foreach($options as $k => $v){
			if(!is_array($v)) {
				self::$options[ $k ] = $v;
			} else {
				foreach($v as $k1=>$v1){
					if(!is_array($v1)) {
						self::$options[ $k ][ $k1 ] = $v1;
					} else {
						foreach($v1 as $k2 => $v2){
							self::$options[ $k ][ $k1 ][$k2] = $v2;
						}
					}
				}
			}
		}
	}

}